/**
 *Name : Snake Game
 *Input : Open URL and give level and the snake color
 *Output:Snake Game
 *author:abhiyan timilsina
**/

//Selecting the required DOM elements
var canvas = document.getElementById('snake_game');
var levels = document.getElementsByClassName('levelButton');
var color = document.getElementById('c');
var colors = ['red','blue','green','orange'];
var wall = document.getElementById('wall');
var goat = document.getElementById('food');
var start = document.getElementById('start');
var pause = document.getElementById('pause');

//To start the game
start.addEventListener('click',()=>{
   gameLoop();
});

//Event Listner for pause
pause.addEventListener('click',()=>{
    displayMessage("PAUSED");
   clearInterval(myInt);
});

//Initializing game parameters
var level =1;
var levelList = [100,80,40];
myInt = null;
var audio = new Audio('./assets/images/eat.mp3');
var bgMusic = new Audio('./assets/images/background.mp3');
var gameOver = new Audio('./assets/images/end.mp3');
var hiss = new Audio('./assets/images/snakehiss.mp3');

//To repeat background music
bgMusic.addEventListener('ended',function(){
    this.currentTime = 0;
    if(myInt)
    this.play();
});

//Initializing Canvas and snake
var context =canvas.getContext("2d")
var wallPat = context.createPattern(wall,'repeat');
context.shadowColor = 'black'
context.shadowOffsetX = 2;
context.shadowOffsetY = 2;
context.shadowBlur = 10;
var snake = [];
var snakeLength = 15;
var snakeHead = [context.canvas.width/2,context.canvas.height*0.2];
var snakeRadius = 10;
var direction = 39;
var snakeDirection = 'RIGHT';
var prev_tail;
food = [];
snakeColor = 'rgb(255,255,255)';

//Canvas and event initialization
canvas.style.display = 'none';
document.addEventListener('keydown',(event=>{if(keysMap[event.keyCode])direction=event.keyCode}));
var keysMap = {
    37 : 'LEFT', 
    38 : 'UP',
    39 : 'RIGHT',
    40 : 'DOWN'
}

//Looping through level to listen for level click event
for(let k=0;k<levels.length;k++) {
    levels[k].addEventListener('click',(event)=>{
     //Initializing canvas with level element     
    level =parseInt(event.target.getAttribute('l'));
    document.getElementById('init').style.display = 'none';
    document.getElementById('over').style.display = 'none';
    canvas.style.display='block'
    run_game();
    });
}

/*Function to calculate distance between two points in 2D */    
var findDistance  = (pointA, pointB)=>Math.sqrt(Math.pow((pointB[0]-pointA[0]),2) + Math.pow((pointB[1]-pointA[1]),2))

//Function to initialize snake
var initializeSnake = ()=>{
    bgMusic.play();
    snake.push(snakeHead);
    for(let i = 1 ;i<snakeLength;i++) {
      snake.push([snake[i-1][0]-(snakeRadius*2), snake[i-1][1]]); 
    }
    
    context.fillStyle = 'black';
    
   snake.forEach((body,index)=>{
    //Give color red for head 
    if(index==0)
     context.fillStyle='red'
     else
     context.fillStyle=snakeColor;
     //Drawing and filling the circles
     context.beginPath();
     context.arc(body[0],body[1],snakeRadius,0,Math.PI*2);
     context.fill();
     context.strokeStyle='black';
     context.arc(body[0],body[1],snakeRadius,0,Math.PI*2);
     context.stroke() 
   });  
}

//Draw Snake 
var drawSnake = (head)=>{
    snakeHead = head;
    snake.unshift(head);
    prev_tail = snake.pop();
    fillBackground();
    
    //Add food when no food
    if(!food.length) {
        let food_x = Math.floor(Math.random() * (context.canvas.width - snakeRadius*3))+1; 
        let food_y = Math.floor(Math.random() *  (context.canvas.height- snakeRadius*3)) + 1;
        food.push(food_x,food_y)
    }
    context.fillStyle =colors[Math.floor(Math.random()*colors.length+1)];
    context.drawImage(goat,food[0],food[1],30,30);
    
    //Drawing snake in canvas
    snake.forEach((body,index)=>{
        if(index==0)
        context.fillStyle='red'
        else
        context.fillStyle=snakeColor;
        context.beginPath();
        context.arc(body[0],body[1],snakeRadius,0,Math.PI*2);
        context.fill();
        context.strokeStyle='black';
        context.arc(body[0],body[1],snakeRadius,0,Math.PI*2);
        context.stroke(); 
      });
}
//Generate new coordinates for snake
function handleInput(code) {
    let keyPressed =keysMap[parseInt(code)];
    let newHead = [];
    if(keyPressed=='RIGHT' && snakeDirection!='LEFT') {
        newHead=[snakeHead[0]+snakeRadius*2,snakeHead[1]];
        snakeDirection = 'RIGHT';
    }
    else if(keyPressed=='LEFT' && snakeDirection!='RIGHT') {
        newHead=[snakeHead[0]-snakeRadius*2,snakeHead[1]];
        snakeDirection = 'LEFT';
    }
    else if(keyPressed=='UP' && snakeDirection!='DOWN') {
        newHead=[snakeHead[0],snakeHead[1]-snakeRadius*2];
        snakeDirection = 'UP';
    }
    else if(keyPressed=='DOWN' && snakeDirection!='UP') {
        newHead=[snakeHead[0],snakeHead[1]+snakeRadius*2];
        snakeDirection = 'DOWN';
    }
    if(newHead.length)
      snakeHead = newHead
    //Return new head
    return newHead;
}


//Detecting collision
var detectCollision = () =>{
  //Snake collaspe
  if(findDistance(food,snake[0])<=snakeRadius*2+5) {
    audio.play();
    food = [];
    current_tail = snake[snake.length-1];
    vertical_distance = current_tail[1] - prev_tail[1]; 
    horizontal_distance = current_tail[0] - prev_tail[0];
    let newbody = []
    if(vertical_distance<0) {
        newbody = [current_tail[0],current_tail[1]-snakeRadius]
    }
    else {
      newbody = [current_tail[0],current_tail[1]+snakeRadius]
    }
    if(horizontal_distance<0){
      newbody = [current_tail[0]-snakeRadius,current_tail[1]]
    }
    else {
        newbody = [current_tail[0]-snakeRadius,current_tail[1]]
    }
    snakeLength++;
    snake.push(newbody);
  }
  if(snakeHead[0]<10 || snakeHead[0]>=context.canvas.width-10 || snakeHead[1]<10 || snakeHead[1]>=context.canvas.height-10){
     
     clearInterval(myInt);
     displayMessage('GAME OVER');
     bgMusic.pause();
     gameOver.play();   
     setTimeout(()=> window.location.replace(window.location.href),5000)
    
    }
  //Snake collaspe
  snake.forEach((elem,index)=>{
      if(index!=0 && findDistance(snake[0],elem)<=snakeRadius){
        clearInterval(myInt);
        displayMessage('GAME OVER');
        bgMusic.pause();
        gameOver.play();
        setTimeout(()=> window.location.replace(window.location.href),5000)
    }});
}

function gameLoop() {
    //Set loop
    myInt = setInterval(()=>{
        hiss.play();
        let dir = Object.keys(keysMap).find(key => keysMap[key] === snakeDirection)
        let newHead ;
        if(Math.abs(parseInt(dir)-parseInt(direction))!==2)
           newHead = handleInput(direction);
        else
           newHead = handleInput(dir);  
        
        drawSnake(newHead); 
        detectCollision();
        },levelList[level-1]);
}
//Filling the background
var fillBackground = ()=>{
    context.save();
    context.fillStyle='white';
    context.fillRect(0,0,context.canvas.width,context.canvas.height);
    var linGred=context.createLinearGradient(10,10,10,context.canvas.height-10);
    linGred.addColorStop(0,'lightBlue');
    linGred.addColorStop(0.5,'lightGray');
    linGred.addColorStop(1,'skyBlue');
    context.fillStyle=linGred;
    context.fillRect(0,0,context.canvas.width,context.canvas.height);  
    context.restore();
    context.save();
    context.strokeStyle= wallPat;
    context.lineWidth=10;
    context.strokeRect(0,0,context.canvas.width,context.canvas.height);
    context.restore();
}
//Display Message
var displayMessage = (string)=>{
    context.font = '60px cursive';
    context.save();
    context.shadowColor = 'white';
    context.fillStyle = 'red';
    let textSize =context.measureText(string).width;
    console.log(textSize);
    context.fillText(string,context.canvas.width/2-textSize/2,context.canvas.height/2);
    context.restore();
}

//main game loop
var run_game = ()=>{
    snakeColor  = color.value;
    console.log(snakeColor);
  //If context is true than start the game  
  if(context) {
    initializeSnake();
    gameLoop();
  }
}